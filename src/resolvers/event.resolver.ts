import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { Between, Not } from 'typeorm';

import RepoService from '../repo/repo.service';
import Event from '../db/models/event.entity';
import EventInput from './input/event.input';
import User from 'src/db/models/user.entity';
import Guest from 'src/db/models/guest.entity';

@Resolver(() => Event)
export default class EventResolver {
  constructor(private readonly repoService: RepoService) {}

  @Query(() => [Event])
  public async getEvents(@Args('userId') userId: number): Promise<Event[]> {
    let events = await this.repoService.eventRepo.find();
    events = events.map((event) => ({
      ...event,
      isGuest: !!(event.userId !== userId),
    }));

    return events;
  }

  @Query(() => [Event])
  public async getEventsFromUser(
    @Args('userId') userId: number,
    @Args('begin') begin: string,
    @Args('end') end: string,
  ): Promise<Event[]> {
    let events = await this.repoService.eventRepo.find({
      where: {
        userId,
        startAt: Between(begin, end),
        endAt: Between(begin, end),
      },
    });

    events = events.map((event) => ({
      ...event,
      isGuest: !!(event.userId !== userId),
    }));

    return events;
  }

  @Query(() => Event, { nullable: true })
  public async getEvent(
    @Args('id') id: number,
    @Args('userId') userId: number,
  ): Promise<Event> {
    const event = await this.repoService.eventRepo.findOne(id);

    return { ...event, isGuest: !!(event.userId !== userId) };
  }

  @Mutation(() => Event)
  public async createEvent(
    @Args('data') input: EventInput,
    @Args('guests') guests: string,
  ): Promise<Event> {
    const event = this.repoService.eventRepo.create({
      title: input.title,
      description: input.description,
      startAt: input.startAt,
      endAt: input.endAt,
      tz: input.tz,
      userId: input.userId,
    });
    await this.repoService.eventRepo.save(event);

    if (guests) {
      const list = JSON.parse(guests);
      await new Promise(() => {
        list.map(async (guest: string) => {
          const user = await this.repoService.userRepo.findOne({
            where: { email: guest },
          });
          if (user) {
            const guest = this.repoService.guestRepo.create({
              eventId: event.id,
              userId: user.id,
            });
            return await this.repoService.guestRepo.save(guest);
          }
        });
      });
    }

    return event;
  }

  @Mutation(() => Event)
  public async updateEvent(
    @Args('id') id: number,
    @Args('data') input: EventInput,
    @Args('guests') guests: string,
  ): Promise<Event> {
    const event = await this.repoService.eventRepo.findOneOrFail(id, {
      relations: ['guestConnection'],
    });
    await this.repoService.eventRepo.save({
      id: id,
      title: input.title,
      description: input.description,
      startAt: input.startAt,
      endAt: input.endAt,
      tz: input.tz,
      userId: input.userId,
    });

    if (event.guestConnection || guests) {
      const pendingList: string[] = [];
      const deleteList: Guest[] = [];

      const list: string[] = JSON.parse(guests);
      const guestList = await this.repoService.guestRepo.find({
        where: { eventId: id },
        relations: ['userConnection'],
      });

      list.forEach((item) => {
        const found = guestList.find(
          (guest) => guest.userConnection.email === item,
        );

        if (!found) {
          pendingList.push(item);
        }
      });

      guestList.forEach((guest) => {
        const found = list.find((item) => item === guest.userConnection.email);

        if (!found) {
          deleteList.push(guest);
        }
      });

      await this.repoService.guestRepo.remove(deleteList);

      await new Promise(() => {
        pendingList.map(async (guest: string) => {
          const user = await this.repoService.userRepo.findOne({
            where: { email: guest },
          });
          if (user) {
            const guest = this.repoService.guestRepo.create({
              eventId: id,
              userId: user.id,
            });
            return await this.repoService.guestRepo.save(guest);
          }
        });
      });
    }

    return event;
  }

  @Mutation(() => Event)
  public async deleteEvent(@Args('id') id: number): Promise<Event> {
    const event = await this.repoService.eventRepo.findOneOrFail(id);
    const guests = await this.repoService.guestRepo.find({
      where: { eventId: id },
    });
    const copy = { ...event };
    await this.repoService.guestRepo.remove(guests);
    await this.repoService.eventRepo.remove(event);
    return copy;
  }

  @ResolveField(() => User, { name: 'user' })
  public async getUser(@Parent() parent: Event): Promise<User> {
    return this.repoService.userRepo.findOne(parent.userId);
  }

  @ResolveField(() => [String], { name: 'guests', nullable: true })
  public async getGuests(@Parent() parent: Event): Promise<string[]> {
    const guests = await this.repoService.guestRepo.find({
      where: { eventId: parent.id },
      relations: ['userConnection'],
    });
    return guests.map((guest) => guest.userConnection.email);
  }
}
