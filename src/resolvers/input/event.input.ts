import { Field, InputType } from '@nestjs/graphql';

@InputType()
export default class EventInput {
  @Field()
  readonly title: string;

  @Field()
  readonly description: string;

  @Field()
  readonly startAt: string;

  @Field()
  readonly endAt: string;

  @Field()
  readonly tz: string;

  @Field()
  readonly userId: number;
}
